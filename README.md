

# Front-end для сайта `Navitron`

Проект разрабатывается на `html` (сетка `bootsrap`) + [less](http://lesscss.org/), иконочный шрифт [Font Awesome](http://fortawesome.github.io/Font-Awesome/), собирается проект на [Gulp](http://gulpjs.com/). Соглашение по именованию css-селекторов [БЭМ](https://ru.bem.info/method/naming-convention/).


### Начало работы
Для сборки проекта нам потребуются `node.js` (https://nodejs.org/), `npm` (https://www.npmjs.com/package/npm) и `bower` (http://bower.io/).
Внешние фреймворки, библиотеки и плагины для верстки (например `jquery` или `bootstrap` размещаются в каталоге `./bower_components` с помощью `bower install ___ --save`) будут устанавливаться автоматически из зависимостей.

#### Установка окружения
1. Если до сих пор не установлен `node.js`, то качаем [тут](https://nodejs.org/), устанавливаем под свою ОС. `npm`, который должен быть в установщике, ставим тоже.
2. С помощью `npm` устанавливаем [bower](http://bower.io/#install-bower) и [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md):
```bash
$ cd ~
$ npm install -g bower
$ npm install -g gulp
```

#### Установка зависимостей для проекта:

```bash
$ cd ~
$ git clone git@bitbucket.org/xeor_ru/navitron-front-end.git
$ npm install
$ bower install
$ gulp
```

Для добавления в зависимости каких-либо плагинов необходимых для разработки (плагины gulp и пр.), необходимо выполнить из корневой директории проекта:
```bash
$ npm install LIBRARY_NAME --save
```

Для добавления в зависимости каких-либо библиотек для вёрстки используем bower, необходимо выполнить из корневой директории проекта:
```bash
$ bower install LIBRARY_NAME --save
```

---