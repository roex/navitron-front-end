/**
 * Created by Anton on 15.12.2015.
 */
/* =================================
 ===  singlePageNav МЕНЮ        ====
 =================================== */
$('.front-menu').singlePageNav({
    //offset: $('#front-menu').outerHeight(),
    offset: 0,
    filter: ':not(.external)',
    updateHash: false,
    beforeStart: function () {
				console.log('begin scrolling');
        if($('body').hasClass('show-menu')){
            //$('#close-button').trigger('click');
        }
    },
    onComplete: function () {

        if($('body').hasClass('show-menu')){
            console.log('done scrolling');
            $('#close-button').trigger('click');
        }
    }
});