/**
 * Custom scripts
 */

// Required libraries
//= ../../bower_components/jquery/dist/jquery.min.js

// jQuery migrate
//= ../../bower_components/jquery-migrate/jquery-migrate.min.js


// Bootstrap scripts
//= ../../bower_components/bootstrap/js/tab.js
//= ../../bower_components/bootstrap/js/transition.js
//= ../../bower_components/bootstrap/js/collapse.js


// External libraries

// ../../bower_components/gsap/src/uncompressed/TweenMax.js
// ../../bower_components/device.js/lib/device.js
//= ./vendor/jquery.themepunch.plugins.min.js
//= ./vendor/jquery.themepunch.revolution.min.js
//= ./vendor/materialize.js
//= ./vendor/wNumb.js
//= ../../bower_components/nouislider/distribute/nouislider.min.js
//= ../../bower_components/wow/dist/wow.min.js
//= ./vendor/singlepagenav.jquery.js