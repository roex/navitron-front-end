/**
 * Custom scripts
 */


$(document).ready(function () {
    /* =================================
     ===  select                    ====
     =================================== */

    $('select').material_select();

    /* =================================
     ===  select end                ====
     =================================== */

    /* =================================
     ===  wow                       ====
     =================================== */
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 200,          // distance to the element when triggering the animation (default is 0)
            mobile: false,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        }
    );
    wow.init();
    /* =================================
     ===  wow end                   ====
     =================================== */

    /* =================================
     ===  Показать еще              ====
     =================================== */

    function autoHeightAnimate(element, time) {
        var curHeight = element.height(), // Get Default Height
            autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({height: autoHeight}, parseInt(time)); // Animate to Auto Height
    }

    if ($('.nt-why__information-btn').length > 0) {
        $('.nt-why__information-btn').on('click touch', function (e) {
            e.preventDefault();
            var $block = $('.nt-information__outer'),
                animateTime = 500;

            if ($block.height() === 0) {
                //$(this).html('Скрыть текст');
                $(this).fadeOut();
                autoHeightAnimate($block, animateTime);

            } else {
                //$(this).html('Показать весь текст');
                $(this).fadeIn();
                $block.stop().animate({height: '0'}, animateTime);

            }
            return false;
        });
        $('.nt-information__close').on('click touch', function (e) {
            e.preventDefault();
            console.log(123123123)
            $('.nt-why__information-btn').trigger('click');
        });
    }

    /* =================================
     ===  Показать утв              ====
     =================================== */

    /* =================================
     ===  backTop                   ====
     =================================== */

    var backTop = $('.back-top');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            backTop.fadeIn();
        } else {
            backTop.fadeOut();
        }
    });
    backTop.on('click', 'a', function () {
        $('body,html').stop(false, false).animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    /* =================================
     ===  backTop end               ====
     =================================== */

    /* =================================
     ===  Скрол до id               ====
     =================================== */

    $('.scroll-btn').click(function () {
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({scrollTop: $(scroll_el).offset().top}, 800, function(){
                //$('.fl-form-tab a[href="'+scroll_el+'"]').trigger('click');
            }); // анимируем скроолинг к элементу scroll_el
        }

        return false; // выключаем стандартное действие
    });

    /* =================================
     ===  Скрол до id end           ====
     =================================== */
});
//$('select').material_select();

$(function () {

});
